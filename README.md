# Overlapping Profiling Results

We evaluate the capability to overlap gradient compression, DNN computation and gradient communication of HiPress and TorchDDP to demonstrate the ideal pipeline we discuss in Figure 8 of our paper.

## Experimental Setup

We trained a vgg19 network with PowerSGD(rank = 64) enabled for 200 iterations using HiPress and TorchDDP. We used 2 nodes, each with one 1080Ti, interconnected with 1000Mbps network.

## HiPress with overlapping

### Results and Explanation

The following screenshot shows the profiling result of HiPress atop PyTorch with PowerSGD(rank = 64). The profiling file can be found in `profiling_results/hipress_2n2c_rank64.nsys-rep`.

![hipress-2n2c-rank64](./profiling_screenshots/hipress_2n2c_rank64_nsight.png)

As we can see, Stream 7 is for DNN computation (forward and backward), Stream 19 is for gradient compression and decompression, and Stream 80 is for gradient communication. Gradient compression is overlapping with DNN computation, and as soon as gradient communication is finished, HiPress starts gradient communication immediately.

The overlapping is enabled by our task manager we implemented in HiPress.

Due to the overlapping, our training step time in this case is **274ms** in average. Time cost breakdown is shown in the table below.

| Name                                   | Total Time |
| -------------------------------------- | ---------- |
| DNN Computation                        | 168ms      |
| Gradient Communication                 | 185ms      |
| Gradient Compression and Decompression | 62ms       |
| Bubbles                                | 0ms        |

The training log of step time can be found in `step_time/hipress_2n2c_rank64_nsight.txt`.

### Reproducing

You can reproduce the profiling file by running the script in `scripts/hipress/run_2n2c_rank64_profile.sh`, and the step time by running `scripts/hipress/run_2n2c_rank64.sh`. You should adjust the ip address according to your cluster and put `hipress_powersgd_vgg.py` in the right directory (/workspace in my case) before using the script.

## TorchDDP without overlapping

### Results and Explanation

The following screenshot shows the profiling results of TorchDDP with PowerSGD(rank = 64). The profiling file can be found in `profiling_results/torchddp_2n2c_rank64.nsys-rep`.

![torchddp_2n2c_rank64](profiling_screenshots/torchddp_2n2c_rank64_nsight.jpg)

Stream 7 is for DNN computation, Stream 16 is for gradient communication, Stream 50, 74, 70, 68, 62, 56, 64, 76, 18, 52, 58, 48, 78, 54, 60, 66, 72 are for gradient compression and decompression. These streams are not overlapped, causing a great increase in training step time, which is **904ms** in average. Time cost breakdown is shown in the table below.

| Name                                   | Total Time |
| -------------------------------------- | ---------- |
| DNN Computation                        | 169ms      |
| Gradient Communication                 | 299ms      |
| Gradient Compression and Decompression | 148ms      |
| Bubbles                                | 266ms      |



The training log of step time can be found in `step_time/torchddp_2n2c_rank64_nsight.txt`.

Since TorchDDP implemented gradient compression in hook and it lacked dependency tracking ability, gradient communication and gradient compression, as well as DNN computation, cannot overlap.

### Reproducing

You can reproduce the profiling file by running `scripts/torchddp/run_2n2c_rank64_profile.sh`, and the step time by running `scripts/torchddp/run_2n2c_rank64.sh`. You should adjust the ip address according to your cluster and put `torchddp_powersgd_vgg.py` in the right directory (/workspace in my case) before using the script.